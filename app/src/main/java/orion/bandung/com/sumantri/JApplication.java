package orion.bandung.com.sumantri;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.text.NumberFormat;

public class JApplication extends Application {
    public DBConn dbConn;
    public View ViewBerandaTmp = null;
    public View ViewTiketTmp = null;

    public static final String TAG = JApplication.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private static JApplication mInstance;
    public final static NumberFormat fmt = NumberFormat.getInstance();
    public boolean isLogOn;
    private String VersionName;
    private int VersionCode;

    @Override
    public void onCreate() {
        super.onCreate();
        this.dbConn = new DBConn(getApplicationContext());
        this.isLogOn = false;
        mInstance = this;
        GetVersion();
    }

    private void GetVersion(){
        PackageInfo pinfo = null;
        try {
            this.VersionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            this.VersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static synchronized JApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public String getVersionName() {
        return VersionName;
    }

    public void setVersionName(String versionName) {
        VersionName = versionName;
    }

    public int getVersionCode() {
        return VersionCode;
    }

    public void setVersionCode(int versionCode) {
        VersionCode = versionCode;
    }
}
