package orion.bandung.com.sumantri;

import android.os.Parcel;
import android.os.Parcelable;

public class ChecklistModel {
    private int id;
    private String name;
    private String items;
    private boolean checklistCompletionStatus;

    public ChecklistModel(int id, String name, String items, boolean checklistCompletionStatus) {
        this.id = id;
        this.name = name;
        this.items = items;
        this.checklistCompletionStatus = checklistCompletionStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public boolean isChecklistCompletionStatus() {
        return checklistCompletionStatus;
    }

    public void setChecklistCompletionStatus(boolean checklistCompletionStatus) {
        this.checklistCompletionStatus = checklistCompletionStatus;
    }
}
