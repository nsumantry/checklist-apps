package orion.bandung.com.sumantri;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private RecyclerView RcvData;
    private ChecklistAdapter ChecklistAdapter;
    private List<ChecklistModel> ListData = new ArrayList<>();
    private Button btnAdd;
    private ProgressDialog Loading;
    private final String Token ="eyJhbGciOiJIUzUxMiJ9.eyJyb2xlcyI6W119.i2OVQdxr08dmIqwP7cWOJk5Ye4fySFUqofl-w6FKbm4EwXTStfm0u-sGhDvDVUqNG8Cc7STtUJlawVAP057Jlg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CreateView();
        InitClass();
        EventClass();
        LoadData();
    }

    private void CreateView() {
        btnAdd   = (Button) findViewById(R.id.btnAdd);
        RcvData   = (RecyclerView) findViewById(R.id.RcvData);

    }

    private void InitClass() {
        getSupportActionBar().setTitle("List");
        this.Loading = new ProgressDialog(MainActivity.this);
        ChecklistAdapter = new ChecklistAdapter(MainActivity.this, ListData);
        RcvData.setLayoutManager(new GridLayoutManager(MainActivity.this, 1, GridLayoutManager.VERTICAL, false));
        RcvData.setAdapter(ChecklistAdapter);
    }

    private void EventClass() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(MainActivity.this, AddItem.class);
                startActivityForResult(s,1);
            }
        });
        RcvData.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LoadData();
            }
        });
    }

    public void LoadData() {
        Loading.setMessage("Loading...");
        Loading.setCancelable(false);
        Loading.show();

        String URL = "http://18.141.178.15:8080/checklist";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String status = new JSONObject(response).getString("statusCode");
                            if (status.equals("2100")) {

                                List<ChecklistModel> itemDataModels = new ArrayList<>();
                                ChecklistModel Data;
                                itemDataModels.clear();

                                JSONArray ArrResults = new JSONObject(response).getJSONArray("data");
                                for (int i = 0; i < ArrResults.length(); i++) {
                                    try {
                                        JSONObject obj = ArrResults.getJSONObject(i);

                                        Data = new ChecklistModel(
                                                obj.getInt("id"),
                                                obj.getString("name"),
                                                obj.getString("name"),
                                                obj.getBoolean("checklistCompletionStatus")
                                        );
                                        itemDataModels.add(Data);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Toast.makeText(MainActivity.this, "Gagap Proses", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                ChecklistAdapter.removeAllModel();
                                ChecklistAdapter.addModels(itemDataModels);
                                Loading.dismiss();
                            } else if (status.equals("Gagap Proses")) {
                                Loading.dismiss();
                                Toast.makeText(MainActivity.this, "Gagap Proses", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Loading.dismiss();
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "Gagap Proses", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Loading.dismiss();
                        Toast.makeText(MainActivity.this, "Gagap Proses", Toast.LENGTH_SHORT).show();
                    }
                }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer " + Token);
                    return params;
            }
        };
        JApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadData();
    }
}
