package orion.bandung.com.sumantri;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConn extends SQLiteOpenHelper {
    public DBConn(Context context) {
        super(context, "e_tiket.db", null, 2);
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "CREATE TABLE IF NOT EXISTS login(" +
                        "id integer," +
                        "user_id varchar(255), " +
                        "email varchar(255), " +
                        "no_telpon varchar(255), " +
                        "pswd varchar(255) " +
                     ")";
        sqLiteDatabase.execSQL(sql);
        this.onUpgrade(sqLiteDatabase, 0, 1);
    }


    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
