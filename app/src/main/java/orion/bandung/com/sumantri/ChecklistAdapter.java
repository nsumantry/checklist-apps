package orion.bandung.com.sumantri;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class ChecklistAdapter extends RecyclerView.Adapter{
    Context context;
    List<ChecklistModel> CheckListModels;
    private int CurrentView;
    MainActivity activity;


    public ChecklistAdapter(Context context, List<ChecklistModel> CheckListModels) {
        this.context = context;
        this.CheckListModels = CheckListModels;
    }

    public void addModels(List<ChecklistModel> CheckListModels) {
        int pos = this.CheckListModels.size();
        this.CheckListModels.addAll(CheckListModels);
        notifyItemRangeInserted(pos, CheckListModels.size());
    }

    public void addMoel(ChecklistModel CheckListModel) {
        this.CheckListModels.add(CheckListModel);
        notifyItemRangeInserted(CheckListModels.size()-1,CheckListModels.size()-1);
    }

    public void removeMoel(int idx) {
        if (CheckListModels.size() > 0){
            this.CheckListModels.remove(CheckListModels.size()-1);
            notifyItemRemoved(CheckListModels.size());
        }
    }

    public void removeAllModel(){
        int LastPosition = CheckListModels.size();
        this.CheckListModels.removeAll(CheckListModels);
        notifyItemRangeRemoved(0, LastPosition);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.item_checklist, parent, false);
        return new ChecklistAdapter.ItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final ChecklistModel mCurrentItem = CheckListModels.get(position);
        final ChecklistAdapter.ItemHolder itemHolder = (ChecklistAdapter.ItemHolder) holder;

        itemHolder.txtItem.setText(mCurrentItem.getItems());
        itemHolder.txtName.setText(mCurrentItem.getName());

    }

    @Override
    public int getItemCount() {
        return CheckListModels.size();
    }

    private class ItemHolder extends RecyclerView.ViewHolder {
        TextView txtName, txtItem;
        CardView crdView;

        public ItemHolder(View itemView) {
            super(itemView);
            txtName  = itemView.findViewById(R.id.txtName);
            txtItem  = itemView.findViewById(R.id.txtItem);
            crdView   = itemView.findViewById(R.id.crdView);
        }
    }
}