package orion.bandung.com.sumantri;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddItem extends AppCompatActivity {
    private EditText txtName, txtItem;
    private Button btnSave;
    private final String Token ="eyJhbGciOiJIUzUxMiJ9.eyJyb2xlcyI6W119.i2OVQdxr08dmIqwP7cWOJk5Ye4fySFUqofl-w6FKbm4EwXTStfm0u-sGhDvDVUqNG8Cc7STtUJlawVAP057Jlg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        CreateView();
        InitClass();
        EventClass();
    }

    private void CreateView() {
        txtName   = (EditText) findViewById(R.id.txtName);
        btnSave   = (Button) findViewById(R.id.btnSave);
    }

    private void InitClass() {
        txtName.setText("");
    }

    private void EventClass() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsSaved();
            }
        });
    }

    private void IsSaved(){
        String url = "http://18.141.178.15:8080/checklist";
        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    String status = new JSONObject(response).getString("statusCode");
                    if (status.equals("2000")) {//
                        Toast.makeText(AddItem.this, "Berhasil simpan", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddItem.this, "Gagap Proses", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(AddItem.this, "Gagap Proses", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AddItem.this, "Gagap Proses", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public String getBodyContentType()
            {
                return "application/json; charset=utf-8";
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + Token);
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                JSONObject json = new JSONObject();
                try {
                    json.put("name", txtName.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return json.toString().getBytes();
            }

//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//
//
//                JSONObject json = new JSONObject();
//                try {
//                    json.put("name", txtName.getText().toString());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                params.put("Body", "application/json; charset=utf-8 " + json.toString());
//                return params;
//            }

        };
        JApplication.getInstance().addToRequestQueue(strReq, FungsiGeneral.tag_json_obj);
    }
}